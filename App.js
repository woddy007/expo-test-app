import {Provider} from 'react-redux';
import React, {Component} from 'react';
import {View, ActivityIndicator, StatusBar, AsyncStorage} from 'react-native';
import {PersistGate} from 'redux-persist/src/integration/react';
import {store, persistor} from './src/redux/store';
import AppView from './src/modules/AppViewContainer';
import colors from "./src/styles/colors";
import DropdownAlert from "react-native-dropdownalert";
import { DropDownHolder } from './src/components/DropDownAlert'

class App extends Component {
    constructor(props) {
        super(props);

        this.state = {

        }
    }

    render() {
        return (
            <Provider store={store}>
                <PersistGate
                    loading={
                        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
                            <ActivityIndicator
                                animating
                                size="large"
                                color={colors.primary}
                            />
                        </View>
                    }
                    persistor={persistor}
                >
                    <StatusBar barStyle="dark-content" backgroundColor="#F3F5F7"/>

                    <AppView/>
                </PersistGate>

                <DropdownAlert ref={ref => DropDownHolder.setDropDown(ref)} closeInterval={1500} translucent updateStatusBar={false}/>
            </Provider>
        );
    }
}

export default App
