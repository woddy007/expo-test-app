import React, { Component } from 'react';
import {compose, lifecycle} from 'recompose';
import {connect} from "react-redux";
import AppNavigator from './RootNavigation';

import { getCountCart } from '../cart/cartProduct/CartProductState'

class NavigatorView extends Component {
    render() {
        let params = {
            countCart: getCountCart()
        }

        return(
            <AppNavigator
                screenProps={params}
            />
        )
    }
}

export default compose(
    connect(
        state => ({
            cart: state.cart
        }),
    ),
)(NavigatorView);
