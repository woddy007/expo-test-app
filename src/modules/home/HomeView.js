import React, {Component} from 'react';
import {
    StyleSheet,
    View,
    Text,
    ScrollView,
    Image,
    Dimensions,
    TouchableWithoutFeedback,
    FlatList,
    SafeAreaView,
    ActivityIndicator,
    RefreshControl
} from 'react-native';
import CardProduct from "../../components/CardProduct";
import axios from "../../plugins/axios";
import {DropDownHolder} from "../../components/DropDownAlert";

const logo = require('../../../assets/logo.png')

const category4 = require('../../../assets/category/category-4.png')
const category15 = require('../../../assets/category/category-15.png')
const category16 = require('../../../assets/category/category-16.png')
const category17 = require('../../../assets/category/category-17.png')
const category18 = require('../../../assets/category/category-18.png')
const category19 = require('../../../assets/category/category-19.png')
const category20 = require('../../../assets/category/category-20.png')


class Home extends Component {
    constructor(props) {
        super(props);

        this.state = {
            loadList: true,

            activeIndexCategory: 17,


            listCategories: [
                {id: "17", name: "Чипсы фруктовые", image: category17},
                {id: "4", name: "Смоква", image: category4},
                {id: "16", name: "Батончики лёгкие", image: category16},
                {id: "15", name: "Батончики питательные", image: category15},
                {id: "18", name: "Чипсы овощные", image: category18},
                {id: "19", name: "Наборы", image: category19},
                {id: "20", name: "Чипсы ягодные", image: category20}
            ],
            listProducts: [],
            productsHeaders: {
                page: 1,
                totalPage: 0
            }
        }
    }

    componentDidMount = () => {
        this.initCategories()
    }

    wait = (timeout) => {
        return new Promise(resolve => {
            setTimeout(resolve, timeout);
        });
    }

    initCategories = () => {
        let list = this.state.listCategories

        this.setState({
            activeIndexCategory: Number(list[0]['id']),
            activeIndexCategoryName: list[0]['name'],
        })

        console.log('state: ', this.state)

        this.loadListProducts()
    }
    changeCategory = (activeIndexCategory) => {
        if (activeIndexCategory != this.state.activeIndexCategory) {
            this.setState({
                activeIndexCategory: activeIndexCategory.id,
                activeIndexCategoryName: activeIndexCategory.name,
                listProducts: [],
                loadList: true
            })

            this.wait(1000).then(() => {
                this.loadListProducts(activeIndexCategory)
            })
        }
    }

    loadListProducts = () => {
        let params = this.getParamsProduct()

        console.log('params: ', params)

        axios('get', 'api-shop-shopapp/getproducts' + params).then(response => {
            let list = this.state.listProducts.concat(response.data)
            let headers = response.headers
            let productsHeaders = this.state.productsHeaders

            productsHeaders['page'] = headers['x-pagination-current-page']
            productsHeaders['totalPage'] = headers['x-pagination-page-count']

            this.setState({
                listProducts: list,
                productsHeaders,
                loadList: false
            })
        }).catch(error => {
        })
    }
    getParamsProduct = () => {
        let params = [
            'category_id=' + this.state.activeIndexCategory,
            'page=' + this.state.productsHeaders.page
        ]

        return '?' + params.join('&')
    }

    renderFooter = () => {
        return ((this.state.page < this.state.totalPage) || this.state.loadList) ?
    <
        ActivityIndicator
        size = "large"
        color = '#C3D600'
            / >
    : <
        View / >
    }
    onEndReached = () => {
        this.wait(1000).then(() => {
            if (Number(this.state.productsHeaders.totalPage) >= this.state.productsHeaders.page + 1) {
                let productsHeaders = this.state.productsHeaders

                productsHeaders['page'] = this.state.productsHeaders.page + 1

                this.setState({
                    productsHeaders
                })

                this.loadListProducts()
            }
        });
    }

    fastBuyProduct = (item) => {
        axios('get', 'api-shop-shopapp/product?id=' + item.id).then(response => {
            this.props.addProduct(response.data)
            DropDownHolder.dropDown.alertWithType('success', 'Успешно', 'Вы добавили товар в корзину.');
        }).catch(error => {
            DropDownHolder.dropDown.alertWithType('error', 'Ошибка', 'Не удалось добавить товар в корзину.');
        })
    }


    render() {
        const widthScreen = Dimensions.get('window').width;
        return (
            < View
        style = {styles.page} >
            < ScrollView >
            < View
        style = {[styles.tabsContent,
        {
            marginTop: this.state.height
        }
    ]
    }
    >
    <
        ScrollView
        style = {styles.tabs}
        horizontal
        showsHorizontalScrollIndicator = {false}
        contentContainerStyle = {
        {
            padding: 10
        }
    }
    >
        {
            this.state.listCategories.map((item, idx) => {
                return (
                    < TouchableWithoutFeedback
                onPress = {() => this.changeCategory(item)}
                key = {'category-' +idx}
                    >
                    < View
                style = {[styles.tab,
                {
                    width: (widthScreen / 4)
                }
            ]
            }
            >
            <View
                style = {[styles.tabImageContent,(this.state.activeIndexCategory == item.id) ? styles.tabImageContentActive : '']}
            >
            <Image
                source={item.image}
                resizeMode={'contain'}
                style={styles.tabImage}
                />
            </View>

                < Text
                style = {styles.tabLabel} > {item.name} < /Text>
                    < /View>
                    < /TouchableWithoutFeedback>
            )
            })
        }
    <
        /ScrollView>
        < /View>
        < SafeAreaView
        style = {
        {
            flex: 1
        }
    }>
    <
        FlatList
        data = {this.state.listProducts}
        renderItem = {({item, index}) => <
        CardProduct
        item = {item}
        categoryName = {this.state.activeIndexCategoryName}
        props = {this.props}
        index = {index}
        fastBuyProduct = {(item) => this.fastBuyProduct(item)}
        />
    }
        keyExtractor = {(item, idx)=>idx}
        showsVerticalScrollIndicator = {false}
        ListFooterComponent = {() => this.renderFooter()}
        numColumns = {2}
        onEndReached = {() => this.onEndReached()}
        style = {styles.scrollProducts}
        />
        < /SafeAreaView>
        < /ScrollView>
        < /View>
    )
        ;
    }

    static navigationOptions = ({navigation}) => {
        return {
            headerTitle: () => {
                return (
                    < View
                style = {styles.navigationContent} >
                    < Image
                style = {styles.navigationLogo}
                source = {logo}
                resizeMode = "contain"
                    / >
                    < /View>
            )
            },
            headerStyle: {
                backgroundColor: '#C3D600',
                borderBottomWidth: 0,
                elevation: 0,
                shadowOpacity: 0,
            },
        };
    };
}

const styles = StyleSheet.create({
    page: {
        flex: 1,
    },

    navigationLogo: {
        height: 40
    },
    navigationContent: {
        alignItems: 'center',
        flex: 1
    },

    tabsContent: {},
    tabs: {},
    tab: {
        alignItems: 'center',
        backgroundColor: 'white'
    },
    tabImageContent: {
        width: 60,
        height: 60,
        position: 'relative',
        marginBottom: 5,
        borderRadius: 50,
        overflow: 'hidden'
    },
    tabImageContentActive: {
        borderWidth: 1,
        borderColor: '#C3D600',
        borderStyle: 'solid',

        shadowOffset: {
            width: 60,
            height: 60,
        },
        shadowOpacity: 1,
        shadowRadius: 10,
        elevation: 15,
        shadowColor: '#C3D600'
    },
    tabImage: {
        width: '100%',
        height: '100%',
    },
    tabLabel: {
        fontSize: 12,
        textAlign: 'center',
    },

    scrollProducts: {
        paddingRight: 8,
        paddingVertical: 10,
        flex: 1,
        paddingTop: 0
    },
})

export default Home
