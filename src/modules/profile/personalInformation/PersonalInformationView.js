import React, {Component} from 'react';
import {
    StyleSheet,
    View,
    Text,
    ScrollView,
    TouchableOpacity,
    TextInput,
    TouchableWithoutFeedback,
    KeyboardAvoidingView,
    Modal,
    ActivityIndicator
} from 'react-native';
import common from "../../../styles/common";
import {TextInputMask} from "react-native-masked-text";
import axios from "../../../plugins/axios";


class PersonalInformation extends Component {
    constructor(props) {
        super(props);

        this.state = {
            fullName: '',
            phone: '',
            street: '',
            dopInfo: '',

            changeValue: false,
            modalVisible: false,
            modalText: 'Записываем данные...'
        }
    }

    componentDidMount = () => {
        let {user} = this.props.user

        this.setState({
            fullName: user.name,
            phone: user.phone,
            street: user.address,
            dopInfo: user.account_details
        })

        this.clickSave = null
    }

    save = () => {
        if (this.state.changeValue) {
            this.setState({
                modalVisible: true
            })

            if (this.clickSave) {
                clearTimeout(this.clickSave)
            }

            this.clickSave = setTimeout(() => {
                let data = {}

                if (this.state.fullName) {
                    data['name'] = this.state.fullName
                }
                if (this.state.street) {
                    data['address'] = this.state.street
                }
                if (this.state.dopInfo) {
                    data['account_details'] = this.state.dopInfo
                }

                axios('put', 'api-shop-shopapp/edituser', data).then(response => {
                    this.setState({
                        modalText: 'Обновляем данные...'
                    })
                    this.whiteUserData()
                }).catch(error => {
                    this.setState({
                        modalVisible: false
                    })
                })
            }, 100)
        }
    }
    whiteUserData = () => {
        axios('get', 'api-shop-shopapp/me').then(response => {
            let user = response.data
            this.props.setUser(user)

            this.setState({
                modalVisible: false,
                modalText: 'Записываем данные...'
            })
        }).catch(error => {
            this.setState({
                modalVisible: false,
                modalText: 'Записываем данные...'
            })
        })
    }

    render() {
        return (
            <KeyboardAvoidingView
                behavior="position"
                enabled
                style={styles.page}
            >
                <ScrollView
                    showsVerticalScrollIndicator={false}
                    contentContainerStyle={{padding: 15}}
                >
                    <TextInput
                        value={this.state.fullName}
                        style={[common.input, styles.input]}
                        placeholder="ФИО"
                        onChangeText={(fullName) => {
                            this.setState({fullName, changeValue: true})
                        }}
                        placeholderTextColor="#D5D5D5"
                    />
                    <TextInputMask
                        type={'custom'}
                        options={{
                            mask: '+7(999)999-99-99',
                        }}
                        value={this.state.phone}
                        style={[common.input, styles.input]}
                        placeholder="Контактный телефон"
                        onChangeText={(phone) => {
                            this.setState({phone, changeValue: true})
                        }}
                        placeholderTextColor="#D5D5D5"
                        keyboardType="number-pad"
                    />

                    <Text style={[styles.text, {marginBottom: 15}]}>Адрес доставки и стандартный комментарий к
                        заказу</Text>

                    <TextInput
                        value={this.state.street}
                        style={[common.input, styles.input]}
                        placeholder="Улица, дом"
                        onChangeText={(street) => {
                            this.setState({street, changeValue: true})
                        }}
                        placeholderTextColor="#D5D5D5"
                        multiline
                    />
                    <TextInput
                        value={this.state.dopInfo}
                        style={[common.input, styles.input]}
                        placeholder="Квартира, домофон и любая другая дополнительная информация"
                        onChangeText={(dopInfo) => {
                            this.setState({dopInfo, changeValue: true})
                        }}
                        placeholderTextColor="#D5D5D5"
                        multiline
                    />

                    <TouchableWithoutFeedback
                        style={styles.button}
                        onPress={() => this.save()}
                    >
                        <View style={[styles.buttonSave]}>
                            <Text style={styles.buttonSaveText}>Сохранить</Text>
                        </View>
                    </TouchableWithoutFeedback>
                </ScrollView>

                <Modal
                    transparent={false}
                    visible={this.state.modalVisible}
                >
                    <View style={styles.modalView}>
                        <ActivityIndicator
                            size="large"
                            color="white"
                        />
                        <Text style={styles.modalText}>{ this.state.modalText }</Text>
                    </View>
                </Modal>
            </KeyboardAvoidingView>
        );
    }

    static navigationOptions = ({navigation}) => {
        return {
            headerTitle: 'Личная информация',
        };
    };
}

const styles = StyleSheet.create({
    page: {
        flex: 1,
    },
    input: {
        marginBottom: 12
    },
    text: {
        color: '#666666',
        fontSize: 16
    },

    buttonSave: {
        width: '100%',
        padding: 10,
        borderRadius: 3,
        backgroundColor: '#C3D600',
        alignItems: 'center'
    },
    buttonSaveText: {
        color: '#FFFFFF',
        fontSize: 18
    },

    modalView: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#C3D600'
    },
    modalText: {
        fontSize: 20,
        marginTop: 5,
        color: 'white'
    }
})

export default PersonalInformation
