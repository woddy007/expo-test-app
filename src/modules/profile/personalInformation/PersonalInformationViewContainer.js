// @flow
import { compose } from 'recompose';
import { connect } from 'react-redux';

import PersonalInformationView from './PersonalInformationView';
import { setUser } from '../personalArea/PersonalAreaState'

export default compose(
  connect(
    state => ({
        user: state.user
    }),
    dispatch => ({
        setUser: user => dispatch(setUser(user))
    }),
  ),
)(PersonalInformationView);
