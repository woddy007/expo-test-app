const UPDATE_USER = 'personalArea/UPDATE_USER'

const initialState = {
    user: null
};

export function setUser(user) {
    return {
        type: UPDATE_USER,
        user
    }
}

export function userExit() {
    let user = initialState.user

    user = null

    return {
        type: UPDATE_USER,
        user
    }
}

export default function UserInfo(state = initialState, action = {}) {
    switch (action.type) {
        case UPDATE_USER:{
            let user = action.user

            return {
                ...state,
                user: user
            }
        }
        default:
            return state;
    }
}
