// @flow
import { compose } from 'recompose';
import { connect } from 'react-redux';

import VerificationView from './VerificationView';

export default compose(
  connect(
    state => ({}),
    dispatch => ({}),
  ),
)(VerificationView);
