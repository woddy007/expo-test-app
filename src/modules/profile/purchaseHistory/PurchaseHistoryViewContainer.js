// @flow
import { compose } from 'recompose';
import { connect } from 'react-redux';

import PurchaseHistoryView from './PurchaseHistoryView';

export default compose(
  connect(
    state => ({}),
    dispatch => ({}),
  ),
)(PurchaseHistoryView);
