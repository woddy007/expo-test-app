import React, {Component} from 'react';
import {
    StyleSheet,
    View,
    Text,
    ScrollView,
    Dimensions,
    Image,
    TouchableWithoutFeedback,
    ActivityIndicator,
    FlatList
} from 'react-native';
import axios from "../../../plugins/axios";
import {render} from "react-native-web";


class PurchaseHistory extends Component {
    constructor(props) {
        super(props);

        this.state = {
            list: [],
            loading: true
        }
    }

    componentDidMount = () => {
        axios('get', 'api-shop-shopapp/history').then(response => {
            this.setState({
                list: response.data,
                loading: false
            })
        }).catch(error => {
        })
    }

    cardHistory = (item, idx) => {
        console.log('item: ', item)

        return (
            <View
                style={styles.cardHistory}
                key={'hostory-' + idx}
            >
                <Text style={styles.cardHistoryTitle}>Заказ № {('000000' + item.id).slice(-6)}</Text>
                <View style={[styles.cardHistoryInfo, styles.cardHistoryInfoColor]}>
                    <Text style={styles.cardHistoryInfoLeft}>Стоимость заказа: </Text>
                    <Text style={styles.cardHistoryInfoRight}>{item.cost}₽</Text>
                </View>
                <View style={styles.cardHistoryInfo}>
                    <Text style={styles.cardHistoryInfoLeft}>Статус: </Text>
                    <Text style={styles.cardHistoryInfoRight}>{item.status_name}</Text>
                </View>
                <View style={[styles.cardHistoryInfo, styles.cardHistoryInfoColor]}>
                    <Text style={styles.cardHistoryInfoLeft}>Способ оплаты: </Text>
                    <Text style={styles.cardHistoryInfoRight}>{item.peyment_method_name}</Text>
                </View>
                <View style={[styles.cardHistoryInfo, { marginBottom: 10 }]}>
                    <Text style={styles.cardHistoryInfoLeft}>Способ доставки: </Text>
                    <Text style={styles.cardHistoryInfoRight}>{item.delivery_method_name_ru}</Text>
                </View>
                <ScrollView
                    horizontal
                    showsHorizontalScrollIndicator={false}
                >
                    {
                        item.products.map((item, idx) => {
                            return this.cardHistoryItem(item, idx)
                        })
                    }
                </ScrollView>
            </View>
        )
    }
    cardHistoryItem = (card, idx) => {
        const widthScreen = Dimensions.get('window').width;

        return (
            <View
                style={[styles.cardHistoryItem, {width: (widthScreen / 3)}]}
                key={'product-' + idx}
            >
                <TouchableWithoutFeedback
                    onPress={() => {
                        this.clickCardItem(card.product_id)
                    }}
                >
                    <View style={{flex: 1}}>
                        <View style={styles.cardHistoryItemImageContent}>
                            <Image
                                style={styles.cardHistoryItemImage}
                                source={{uri: card.image}}
                                resizeMode={'contain'}
                            />
                        </View>
                        <Text style={styles.cardHistoryItemCategory}>{card.category_name}</Text>
                        <Text style={styles.cardHistoryItemName}>{card.product_name}</Text>
                    </View>
                </TouchableWithoutFeedback>
            </View>
        )
    }
    clickCardItem = (id) => {
        let {navigate} = this.props.navigation

        navigate('Product', {
            id
        })
    }

    render() {
        return (
            <View style={styles.page}>
                {
                    this.state.loading && <View style={{padding: 15}}>
                        <ActivityIndicator
                            size="large"
                            color='#C3D600'
                        />
                    </View>
                }
                {
                    (this.state.list.length > 0) ? <ScrollView
                            showsVerticalScrollIndicator={false}
                            contentContainerStyle={{padding: 15}}>
                            {
                                this.state.list.map((item, idx) => {
                                    return this.cardHistory(item, idx)
                                })
                            }
                        </ScrollView>
                        :
                        <View style={styles.blockListEmpty}>
                            <Text style={styles.textListEmpty}>
                                {
                                    (this.state.loading)?'Идет загрузка' : 'Нечего не найдено'
                                }
                            </Text>
                        </View>
                }
            </View>
        );
    }

    static navigationOptions = ({navigation}) => {
        return {
            headerTitle: 'История заказов',
        };
    };
}

const styles = StyleSheet.create({
    page: {
        flex: 1
    },

    cardHistory: {
        flex: 1,
        paddingVertical: 20,
        borderBottomWidth: 1,
        borderStyle: 'solid',
        borderColor: '#F1F1F1'
    },
    cardHistoryTitle: {
        fontSize: 18,
        color: '#666666',
        marginBottom: 5
    },
    cardHistoryInfo: {
        flexDirection: 'row',
        width: '100%',
        padding: 5
    },
    cardHistoryInfoColor: {
        backgroundColor: '#F8F8F8'
    },
    cardHistoryInfoLeft: {
        display: 'flex',
        width: '45%',
        color: '#666666',
        fontWeight: '700',
        fontSize: 14
    },
    cardHistoryInfoRight: {
        width: '55%',
        color: '#666666',
        fontSize: 14
    },


    cardHistoryItem: {
        marginRight: 10,
    },
    cardHistoryItemImageContent: {
        width: '100%',
        height: 100,
        marginBottom: 7,
        borderRadius: 5,
        overflow: 'hidden',
        backgroundColor: '#faf9f5',
        padding: 15
    },
    cardHistoryItemImage: {
        flex: 1,
        width: '100%'
    },
    cardHistoryItemCategory: {
        fontSize: 12,
        fontWeight: '600',
        color: '#666666',
    },
    cardHistoryItemName: {
        fontSize: 14,
        fontWeight: '600',
        color: '#C3D600'
    },


    blockListEmpty: {
        justifyContent: 'center',
        alignContent: 'center'
    },
    textListEmpty: {
        textAlign: 'center',
        fontSize: 21
    },
})

export default PurchaseHistory
