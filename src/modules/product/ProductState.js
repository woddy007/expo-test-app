// @flow
type ProductStateType = {};

type ActionType = {
  type: string,
  payload?: any,
};

export const initialState: ProductStateType = {};

export const ACTION = 'ProductState/ACTION';

export function actionCreator(): ActionType {
  return {
    type: ACTION,
  };
}

export default function ProductStateReducer(state: ProductStateType = initialState, action: ActionType): ProductStateType {
  switch (action.type) {
    case ACTION:
      return {
        ...state,
      };
    default:
      return state;
  }
}
