import {AsyncStorage} from 'react-native';

const UPDATE_LIST = 'cart/UPDATE_LIST'

const initialState = {
    list: {},
    orderInformation: {
        delivery: {
            type: null,
            city: null,
            street: null,
            house: null,
            apartment: null,
        },
        payment: null,
    }
}

export async function writeInitState(basket) {
    try {
        await AsyncStorage.setItem('basket', JSON.stringify(basket));
    } catch (error) {}
}

export function getTotalPrice() {
    let list = initialState.list
    let totalPrice = 0

    for(let key in list){
        let product = list[key]

        totalPrice += Number(product.price) * product.count
    }

    return totalPrice
}

export function addProduct(product, count) {
    let list = initialState.list

    count = (count)? count: 1

    if ( list[product.id] ){
        count = list[product.id]['count'] + count
    }

    list[product.id] = {
        ...product,
        count
    }

    writeInitState(list)

    return {
        type: UPDATE_LIST,
        list
    }
}
export function deleteProduct(id) {
    let list = initialState.list

    delete list[id]

    writeInitState(list)

    return {
        type: UPDATE_LIST,
        list
    }
}
export function changeCountProduct(id, count) {
    let list = initialState.list

    list[id]['count'] = count

    writeInitState(list)

    return {
        type: UPDATE_LIST,
        list
    }
}
export function clearCart() {
    let list = initialState.list

    for(let key in list){
        delete list[key]
    }

    writeInitState(list)

    return {
        type: UPDATE_LIST,
        list
    }
}

export function getCountCart() {
    let list = initialState.list
    let count = 0

    for(let key in list){
        let product = list[key]

        count += product.count
    }

    return count
}

export default function CartState(state = initialState, action = {}) {
    switch (action.type) {
        case UPDATE_LIST: {
            let list = action.list

            return {
                ...state,
                list
            }
        }
        default:
            return state;
    }
}
