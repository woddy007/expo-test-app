// @flow
import { compose } from 'recompose';
import { connect } from 'react-redux';

import CartOrderView from './CartOrderView';
import { clearCart } from '../cartProduct/CartProductState'

export default compose(
  connect(
    state => ({
        cart: state.cart,
        user: state.user,
        catalog: state.catalog,
    }),
    dispatch => ({
        clearCart: () => dispatch(clearCart())
    }),
  ),
)(CartOrderView);
