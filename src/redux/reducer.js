import { combineReducers } from 'redux';

import app from '../modules/home/HomeState'
import catalog from '../modules/home/HomeState'
import cart from '../modules/cart/cartProduct/CartProductState'
// import user from '../modules/profile/personalArea/PersonalAreaState'

const rootReducer = combineReducers({
    app,
    // catalog,
    cart,
    // user
});

export default rootReducer
