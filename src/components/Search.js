import React, {Component} from 'react'
import {
    View,
    StyleSheet,
    TextInput,
    Image,
} from 'react-native';

const search = require('../../assets/icons/search.png')

class Search extends Component {
    constructor(props) {
        super(props)

        this.state = {
            query: ''
        }
    }

    render() {
        return (
            <View style={styles.view}>
                <View style={styles.inputView}>
                    <Image
                        style={styles.inputIcon}
                        source={search}
                    />
                    <TextInput
                        value={this.state.query}
                        style={styles.input}
                        placeholder="Поиск"
                        onChangeText={query => {this.setState({query})}}
                        placeholderTextColor="#DAE853"
                    />
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    view: {home
        backgroundColor: '#C3D600',
        paddingVertical: 10,
        paddingHorizontal: 16
    },
    input: {
        color: 'white',
        flex: 1
    },
    inputIcon: {
        width: 15,
        height: 15,
        tintColor: '#DAE853',
        marginRight: 10
    },
    inputView: {
        backgroundColor: '#B8CA02',
        borderRadius: 3,
        flexDirection: 'row',
        alignItems: 'center',
        padding: 6
    }
})

export default Search
